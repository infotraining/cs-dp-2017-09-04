﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DP.Creational.AbstractFactory.TheoryCode
{
    /// <summary>
    /// Abstract Factory Design Pattern.
    /// </summary>
    class MainApp
    {
        /// <summary>
        /// Entry point into console application.
        /// </summary>
        public static void Main()
        {
            // Abstract factory #1
            IAbstractFactory factory1 = new ConcreteFactory1();
            Client client1 = new Client(factory1);
            client1.Run();

            // Abstract factory #2
            IAbstractFactory factory2 = new ConcreteFactory2();
            Client client2 = new Client(factory2);
            client2.Run();

            // Wait for user input
            Console.Read();
        }
    }

    // "AbstractFactory"
    interface IAbstractFactory
    {
        IProductA CreateProductA();
        IProductB CreateProductB();
    }

    // "ConcreteFactory1"
    class ConcreteFactory1 : IAbstractFactory
    {
        public IProductA CreateProductA()
        {
            return new ProductA1();
        }
        public IProductB CreateProductB()
        {
            return new ProductB1();
        }
    }

    // "ConcreteFactory2"
    class ConcreteFactory2 : IAbstractFactory
    {
        public IProductA CreateProductA()
        {
            return new ProductA2();
        }
        public IProductB CreateProductB()
        {
            return new ProductB2();
        }
    }

    // "AbstractProductA"
    interface IProductA
    {
    }

    // "AbstractProductB"
    interface IProductB
    {
        void Interact(IProductA a);
    }

    // "ProductA1"
    class ProductA1 : IProductA
    {
    }

    // "ProductB1"
    class ProductB1 : IProductB
    {
        public void Interact(IProductA a)
        {
            Console.WriteLine(this.GetType().Name +
                " interacts with " + a.GetType().Name);
        }
    }

    // "ProductA2"
    class ProductA2 : IProductA
    {
    }

    // "ProductB2"
    class ProductB2 : IProductB
    {
        public void Interact(IProductA a)
        {
            Console.WriteLine(this.GetType().Name +
                " interacts with " + a.GetType().Name);
        }
    }

    // "Client" - the interaction environment of the products
    class Client
    {
        private IProductA AbstractProductA;
        private IProductB AbstractProductB;

        // Constructor
        public Client(IAbstractFactory factory)
        {
            AbstractProductB = factory.CreateProductB();
            AbstractProductA = factory.CreateProductA();
        }

        public void Run()
        {
            AbstractProductB.Interact(AbstractProductA);
        }
    }
}
