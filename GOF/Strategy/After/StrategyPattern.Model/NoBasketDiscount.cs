﻿namespace StrategyPattern.Model
{
    public class NoBasketDiscount : IBasketDiscountStrategy 
    {
        public decimal GetTotalCostAfterApplyingDiscountTo(Basket basket)
        {
            return basket.TotalCost; 
        }     
    }
}
