﻿namespace StrategyPattern.Model
{
    public class Basket
    {
        private IBasketDiscountStrategy _basketDiscount;

        public Basket(DiscountType discountType)
        {
            _basketDiscount = BasketDiscountFactory.GetDiscount(discountType);
        }

        public decimal TotalCost { get; set; }        

        public decimal GetTotalCostAfterDiscount()
        {
            return _basketDiscount.GetTotalCostAfterApplyingDiscountTo(this); 
        }
    }
}
