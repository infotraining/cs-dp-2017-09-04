﻿namespace StrategyPattern.Model
{
    public class BasketDiscountPercentageOff : IBasketDiscountStrategy 
    {
        public decimal GetTotalCostAfterApplyingDiscountTo(Basket basket)
        {
            return basket.TotalCost * 0.85m;
        }     
    }
}
