﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;

namespace Drawing
{
    public interface IShape
    {
        void Draw();
        void Move(int dx, int dy);
        IShape Clone();
    }

    [Serializable]
    public abstract class Shape : IShape
    {
        protected List<Point> Points { get; set; } = new List<Point>();

        protected int NumberOfPoints()
        {
            return Points.Count;
        }

        protected void AddPoint(Point pt)
        {
            Points.Add(pt);
        }

        #region IShape Members

        public abstract void Draw();

        public virtual void Move(int dx, int dy)
        {
            for (int i = 0; i < Points.Count; ++i)
            {
                Points[i] = new Point(Points[i].X + dx, Points[i].Y + dy);
            }
        }

        public IShape Clone()
        {
            using (MemoryStream stream = new MemoryStream())
            {
                BinaryFormatter formatter = new BinaryFormatter();
                formatter.Serialize(stream, this);
                stream.Seek(0, SeekOrigin.Begin);

                IShape clonedShape = (IShape)formatter.Deserialize(stream);
                return clonedShape;
            }
        }
        
        #endregion
    }

    public interface IShapeCreator
    {
        IShape CreateShape(params object[] args);        
    }

    public class ShapeFactory
    {
        private IDictionary<string, IShapeCreator> _creators = new Dictionary<string, IShapeCreator>();

        public IShape Create(string id, params object[] args)
        {
            return _creators[id].CreateShape(args);
        }

        public void Register(string id, IShapeCreator creator)
        {
            _creators.Add(id, creator);
        }

        public void Unregister(string id)
        {
            _creators.Remove(id);
        }
    }
}
