﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace Drawing
{
    [Serializable]
    public class Circle : Shape
    {
        public int Radius { get; set; }

        public Circle(int x, int y, int radius)
        {
            AddPoint(new Point(x, y));
            Radius = radius;
        }

        public override void Draw()
        {
            Console.WriteLine("Drawing a circle at {0} with radius {1}", Points[0], Radius);
        }

        //public override IShape Clone()
        //{
        //    return new Circle(Points[0].X, Points[0].Y, Radius);
        //}

        public class Creator : IShapeCreator
        {
            IShape IShapeCreator.CreateShape(params object[] args)
            {
                XElement circleElement = (XElement)args[0];
                int x = int.Parse(circleElement.Element("Point").Element("X").Value);
                int y = int.Parse(circleElement.Element("Point").Element("Y").Value);
                int radius = int.Parse(circleElement.Element("Radius").Value);

                return new Circle(x, y, radius);
            }
        }
    }
}
