﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using Drawing;
using System.Xml.Linq;

namespace DP.Creational.FactoryMethod.Example
{
    public class Document
    {
        private readonly ShapeFactory _shapeFactory;
        List<IShape> _shapes = new List<IShape>();

        public Document(ShapeFactory shapeFactory)
        {
            _shapeFactory = shapeFactory;
        }

        public virtual void Load(string path)
        {
            XElement root = XElement.Load(path);

            foreach (XElement element in root.Elements("Shape"))
            {
                string id = element.Attribute("Id").Value;
                try
                {
                    IShape s = _shapeFactory.Create(id, element);
                    _shapes.Add(s);
                }
                catch (KeyNotFoundException e)
                {
                    Console.WriteLine(e.Message);
                }
            }
        }

        public void Show()
        {
            foreach (IShape s in _shapes)
                s.Draw();
        }
    }

    class Program
    {
        static void InitShapeFactory(ShapeFactory shapeFactory)
        {
            shapeFactory.Register("Circle", new Circle.Creator());
            shapeFactory.Register("Rectangle", new Rectangle.Creator());
        }

        static void Main(string[] args)
        {
            ShapeFactory shapeFactory = new ShapeFactory();
            InitShapeFactory(shapeFactory);
            Document doc = new Document(shapeFactory);
            doc.Load("../../graphics.xml");
            doc.Show();

            // prototype
            IShape shp = GetSelectedShape();
            IShape copyOfShp = shp.Clone();
            copyOfShp.Draw();
        }

        private static Shape GetSelectedShape()
        {
            return new Circle(10, 20, 100);
        }
    }
}
