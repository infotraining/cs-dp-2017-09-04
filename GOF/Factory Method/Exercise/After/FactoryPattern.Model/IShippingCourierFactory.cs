using FactoryPattern.Model.Creators;

namespace FactoryPattern.Model
{
    public interface IShippingCourierFactory
    {
        IShippingCourier CreateShippingCourier(Order order);
        void RegisterSpecifiedCreator(SpecifiedShippingCreator specifiedCreator);
    }
}