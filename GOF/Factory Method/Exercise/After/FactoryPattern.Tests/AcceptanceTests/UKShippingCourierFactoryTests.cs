﻿using FactoryPattern.Model;
using FactoryPattern.Model.Creators;
using FactoryPattern.Model.Specifications;
using NUnit.Framework;

namespace FactoryPattern.Tests.AcceptanceTests
{
    [TestFixture]
    public class UKShippingCourierFactoryTests
    {
        private IShippingCourierFactory _shippingCourierFactory;
        
        [SetUp]
        public void Setup()
        {
            
            _shippingCourierFactory = new UKShippingCourierFactory(new DefaultShippingCourierCreator());
            _shippingCourierFactory.RegisterSpecifiedCreator(
                new SpecifiedShippingCreator() { ShippingCreator = new DHLShippingCreator(), Specification = new TotalCostSpecification().Or(new WeightOver5KGSpecification())});
        }

        [Test]
        public void UKShippingCourierFactory_Should_Create_DHL_Shipping_Courier_For_An_Order_With_A_TotalCost_Of_Over_100()
        {
            Order order = new Order() { TotalCost = 101};

            IShippingCourier courier = _shippingCourierFactory.CreateShippingCourier(order);

            Assert.That(courier, Is.TypeOf(typeof(DHL)));
        }

        [Test]
        public void UKShippingCourierFactory_Should_Create_DHL_Shipping_Courier_For_An_Order_With_A_Weight_In_KG_Over_5()
        {
            Order order = new Order() { WeightInKG = 6 };

            IShippingCourier courier = _shippingCourierFactory.CreateShippingCourier(order);

            Assert.That(courier, Is.TypeOf(typeof(DHL)));
        }

        [Test]
        public void UKShippingCourierFactory_Should_Create_RoyalMail_Shipping_Courier_For_An_Order_With_A_Weight_In_KG_5_And_Under()
        {
            Order order = new Order() { WeightInKG = 5 };

            IShippingCourier courier = _shippingCourierFactory.CreateShippingCourier(order);

            Assert.That(courier, Is.TypeOf(typeof(RoyalMail)));
        }

        [Test]
        public void UKShippingCourierFactory_Should_Create_DHL_Shipping_Courier_For_An_Order_With_A_TotalCost_Of_100_And_Under()
        {
            Order order = new Order() { TotalCost = 100 };

            IShippingCourier courier = _shippingCourierFactory.CreateShippingCourier(order);

            Assert.That(courier, Is.TypeOf(typeof(RoyalMail)));
        }
    }
}
