﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FactoryPattern.Model;
using FactoryPattern.Model.Creators;
using Moq;
using NUnit.Framework;
using SpecificationPattern;

namespace FactoryPattern.Tests
{
    [TestFixture]
    public class UKShippingCourierFactoryTests
    {
        private IShippingCourierFactory _shippingCourierFactory;
        private Mock<ISpecification<Order>> _mqSpec;
        private Mock<ICourierCreator> _mqCourierCreator;
        private Mock<ICourierCreator> _mqDefaultCreator;
        
        [SetUp]
        public void Setup()
        {
            _mqSpec = new Mock<ISpecification<Order>>();
            _mqCourierCreator = new Mock<ICourierCreator>();
            _mqDefaultCreator = new Mock<ICourierCreator>();

            var specifiedCreator = new SpecifiedShippingCreator { Specification = _mqSpec.Object, ShippingCreator = _mqCourierCreator.Object };

            _shippingCourierFactory = new UKShippingCourierFactory(_mqDefaultCreator.Object);
            _shippingCourierFactory.RegisterSpecifiedCreator(specifiedCreator);
        }

        [Test]
        public void WhenSpecificationForOrderIsSatisfiedCreatesCourier()
        {
            // Arrange            
            _mqSpec.Setup(mq => mq.IsSatisfiedBy(It.IsAny<Order>())).Returns(true);

            Order order = new Order {TotalCost = 101};

            // Act
            var shippingCourier = _shippingCourierFactory.CreateShippingCourier(order);
            
            // Assert
            _mqCourierCreator.Verify(mq => mq.CreateShippingCourier(), Times.Once);
        }

        [Test]
        public void WhenSpecificationForOrderIsNotSatisfiedCreatesDefaultCourier()
        {
            // Arrange
            _mqSpec.Setup(mq => mq.IsSatisfiedBy(It.IsAny<Order>())).Returns(false);

            Order order = new Order { TotalCost = 101 };

            // Act
            var shippingCourier = _shippingCourierFactory.CreateShippingCourier(order);

            // Assert
            _mqCourierCreator.Verify(mq  => mq.CreateShippingCourier(), Times.Never);
            _mqDefaultCreator.Verify(mq => mq.CreateShippingCourier(), Times.Once);
        } 
    }
}
