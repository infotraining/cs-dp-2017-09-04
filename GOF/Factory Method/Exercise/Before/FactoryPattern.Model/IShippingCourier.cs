﻿namespace FactoryPattern.Model
{
    public interface IShippingCourier
    {
        string GenerateConsignmentLabelFor(Address address);
    }
}
