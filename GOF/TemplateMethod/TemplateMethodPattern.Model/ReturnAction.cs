﻿namespace TemplateMethodPattern.Model
{
    public enum ReturnAction
    {
        FaultyReturn = 0,
        NoQuibblesReturn = 1
    }
}
