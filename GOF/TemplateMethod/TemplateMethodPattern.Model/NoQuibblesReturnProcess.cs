﻿namespace TemplateMethodPattern.Model
{
    public class NoQuibblesReturnProcess : ReturnProcessTemplate
    {
        protected override void GenerateReturnTransactionFor(ReturnOrder ReturnOrder)
        {
            // Code to put items back into stock...
        }

        protected override void CalculateRefundFor(ReturnOrder ReturnOrder)
        {            
            ReturnOrder.AmountToRefund = ReturnOrder.PricePaid; 
        }
    }
}
