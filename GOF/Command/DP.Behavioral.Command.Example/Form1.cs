﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace DP.Behavioral.Command.Example
{
    public partial class FormMain : Form
    {
        ICommand cmdCopy;
        ICommand cmdPaste;
        ICommand cmdToUpper;
        ICommand cmdToLower;
        ICommand cmdRemove;
        ICommand cmdUndo;
        ICommandHistory _commandHistory = new CommandHistory();

        public FormMain()
        {
            InitializeComponent();

            cmdCopy = new CopyCommand(txtDocument);
            cmdPaste = new PasteCommand(txtDocument, _commandHistory);
            cmdToUpper = new ToUpperCommand(txtDocument, _commandHistory);
            cmdRemove = new RemoveCommand(txtDocument, _commandHistory);
            cmdUndo = new UndoCommand(_commandHistory);
        }

        private void btnToUpper_Click(object sender, EventArgs e)
        {
            cmdToUpper.Execute();
        }

        private void btnCopy_Click(object sender, EventArgs e)
        {     
            cmdCopy.Execute();
        }

        private void btnPaste_Click(object sender, EventArgs e)
        {            
            cmdPaste.Execute();
        }

        private void txtDocument_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {
            cmdRemove.Execute();
        }

        private void btnUndo_Click(object sender, EventArgs e)
        {
            try
            {
                cmdUndo.Execute();
            }
            catch (Exception excpt)
            {
                MessageBox.Show("Empty undo list...");
            }
        }

    }
}
