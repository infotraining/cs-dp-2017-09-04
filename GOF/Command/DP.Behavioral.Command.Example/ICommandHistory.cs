﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DP.Behavioral.Command.Example
{
    public interface ICommandHistory
    {
        void Push(ICommand command);
        ICommand Pop();
    }

    public class CommandHistory : ICommandHistory
    {
        Stack<ICommand> _history = new Stack<ICommand>();

        public void Push(ICommand command)
        {
            _history.Push(command);
        }

        public ICommand Pop()
        {
            return _history.Pop();
        }
    }

}
