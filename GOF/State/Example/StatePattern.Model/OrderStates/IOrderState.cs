﻿namespace StatePattern.Model.OrderStates
{
    public interface IOrderState
    {        
        bool CanShip(Order Order);
        void Ship(Order Order);

        bool CanCancel(Order Order);
        void Cancel(Order order);

        OrderStatus Status { get; }
    }
}
