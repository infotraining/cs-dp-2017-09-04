using System;

namespace BankAccount.Model
{
    public class InsufficientFunds : ApplicationException
    {
        public InsufficientFunds(string message) : base(message)
        {
        }
    }
}