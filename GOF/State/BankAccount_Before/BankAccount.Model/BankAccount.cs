using System.Diagnostics.Contracts;

namespace BankAccount.Model
{
    interface IBankAccountState
    {
        void Withdraw(decimal amount, BankAccount bankAccount);
        void PayInterest(BankAccount bankAccount);
        AccountState Status { get; }
    }

    class NormalState : IBankAccountState
    {
        public AccountState Status { get { return AccountState.Normal; } }

        public void PayInterest(BankAccount bankAccount)
        {
            bankAccount.Balance *= 1.1M;
        }

        public void Withdraw(decimal amount, BankAccount bankAccount)
        {
            bankAccount.Balance -= amount;
        }
    }

    class OverdraftState : IBankAccountState
    {
        public AccountState Status => AccountState.Overdraft;

        public void PayInterest(BankAccount bankAccount)
        {
            bankAccount.Balance *= 1.2M;
        }

        public void Withdraw(decimal amount, BankAccount bankAccount)
        {
            throw new InsufficientFunds(string.Format("Insufficient funds on account #1{0}", bankAccount.Id));
        }
    }

    public class BankAccount
    {
        public int Id { get; private set; }
        public decimal Balance { get; internal set; }
        private IBankAccountState _state;

        public AccountState Status => _state.Status;         

        public BankAccount(int id, decimal balance = 0.0M)
        {
            Id = id;
            Balance = balance;            
            _state = UpdateAccountState();
        }

        public void Deposit(decimal amount)
        {
            Contract.Assert(amount > 0.0M);        
            Balance += amount;        
            _state = UpdateAccountState();
        }

        public void Withdraw(decimal amount)
        {
            Contract.Assert(amount > 0.0M);        
            _state.Withdraw(amount, this);            
            _state = UpdateAccountState();
        }

        public void PayInterest()
        {
            _state.PayInterest(this);                
        }

        private IBankAccountState UpdateAccountState()
        {
            if (Balance >= 0)
                return new NormalState();
            else
                return new OverdraftState();                
        }
    }
}