﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DP.Structural.Decorator.Exercise
{
    /// <summary>
    ///  Coffee
    /// </summary>
    public abstract class Coffee
    {
        protected double price;
        protected string description;

        public virtual double GetTotalPrice()
        {
            return price;
        }

        public virtual string GetDescription()
        {
            return description;
        }

        public abstract void Prepare();
    }

    /// <summary>
    /// Espresso
    /// </summary>
    public class Espresso : Coffee
    {
        public Espresso()
        {
            price = 4.0;
            description = "Espresso";
        }

        public override void Prepare()
        {
            Console.WriteLine("Making a perfect Espresso: 7g, 15bar, 95degC & 25sec");
        }
    }

    /// <summary>
    /// Cappuccino
    /// </summary>
    public class Cappuccino : Coffee
    {
        public Cappuccino()
        {
            price = 6.0;
            description = "Cappuccino";
        }

        public override void Prepare()
        {
            Console.WriteLine("Making a perfect Cappuccino");
        }
    }

    /// <summary>
    /// Latte
    /// </summary>
    public class Latte : Coffee
    {
        public Latte()
        {
            price = 8.0;
            description = "Latte";
        }

        public override void Prepare()
        {
            Console.WriteLine("Making a superb Latte");
        }
    }

    public abstract class CoffeeDecorator : Coffee
    {
        protected Coffee _coffee;

        public CoffeeDecorator(Coffee coffee)
        {
            _coffee = coffee;
        }

        public override double GetTotalPrice()
        {
            return _coffee.GetTotalPrice() + base.GetTotalPrice();
        }

        public override string GetDescription()
        {
            return _coffee.GetDescription() + " + " + base.GetDescription();
        }

        public override void Prepare()
        {
            _coffee.Prepare();
        }
    }

    class Whipped : CoffeeDecorator
    {
        public Whipped(Coffee coffee) : base(coffee)
        {
            base.price = 2.5;
            base.description = "Whipped";
        }

        public override void Prepare()
        {
            base.Prepare();
            Console.WriteLine("Adding whipped cream...");
        }
    }

    class Whisky : CoffeeDecorator
    {
        public Whisky(Coffee coffee) : base(coffee)
        {
            base.price = 6.0;
            base.description = "Whisky";
        }

        public override void Prepare()
        {
            base.Prepare();
            Console.WriteLine("Adding whisky...");
        }
    }

    class ExtraEspresso : CoffeeDecorator
    {
        public ExtraEspresso(Coffee coffee) : base(coffee)
        {
            base.price = 4.0;
            base.description = "ExtraEspresso";
        }

        public override void Prepare()
        {
            base.Prepare();
            new Espresso().Prepare();
        }
    }

    // TO DO: Dodatki: cena - Whipped: 2.5, Whiskey: 6.0, ExtraEspresso: 4.0

    // TO DO: Zastapic statyczne klasy EspressoConPanna, CaffeLatteExtraEspresso, IrishCream  
}
