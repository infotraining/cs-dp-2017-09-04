﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DP.Structural.Decorator.Exercise
{
    class Program
    {
        static void Main(string[] args)
        {
            Coffee myCoffee = new Whipped(new Whisky(new Espresso()));

            Console.WriteLine("Coffee: {0} - Price: {1:c}", myCoffee.GetDescription(), myCoffee.GetTotalPrice());
            myCoffee.Prepare();

            Console.ReadKey();
        }
    }
}
