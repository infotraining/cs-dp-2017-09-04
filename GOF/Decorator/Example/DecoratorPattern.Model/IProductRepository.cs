﻿using System.Collections.Generic;

namespace DecoratorPattern.Model
{
    public interface IProductRepository
    {
        IEnumerable<Product> FindAll(); 
    }
}
