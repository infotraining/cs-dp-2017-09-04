﻿namespace DecoratorPattern.Model
{
    public interface IBasketItem
    {
        Product Product { get; set; }
        int Quantity { get; set; }
        decimal LineTotal { get; }
    }
}
