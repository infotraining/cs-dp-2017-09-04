﻿namespace DecoratorPattern.Model
{
    public interface IPrice
    {
        decimal Cost { get; set; }
    }
}
