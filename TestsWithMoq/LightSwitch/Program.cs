﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LedDevices;
using SimpleInjector;

namespace LightSwitching
{
    public interface ISwitch
    {
        bool IsOn { get; set; }
        void On();
        void Off();
    }

    public interface ILogger : IDisposable
    {
        void Log(string msg);
    }

    public interface ILoggerFactory
    {
        ILogger Create();
    }

    public class DbLogger : ILogger
    {
        string connStr;

        public DbLogger(string connectionString)
        {
            connStr = connectionString;
        }

        public void Dispose()
        {           
        }

        public void Log(string msg)
        {
            Console.WriteLine("connecting to db: {0}", connStr);
            Console.WriteLine("logging to db: {0}", msg);
        }
    }

    public class DbLoggerFactory : ILoggerFactory
    {
        public ILogger Create()
        {
            return new DbLogger("localhost:db");
        }
    }

    public class ConsoleLogger : ILogger
    {
        public void Log(string msg)
        {
            Console.WriteLine("Logging: " + msg);
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~ConsoleLogger() {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion
    }

    public class ConsoleLoggerFactory : ILoggerFactory
    {
        public ILogger Create()
        {
            return new ConsoleLogger();
        }
    }


    public class LedSwitch : ISwitch
    {
        LedLight _led;
        ILoggerFactory _loggerFactory;

        public string Id { get; set; }

        public LedSwitch(LedLight led, string id, ILoggerFactory loggerFactory)
        {
            _led = led;
            _loggerFactory = loggerFactory;
            Id = id;
        }

        public bool IsOn { get; set; }

        public void Off()
        {
            using (var logger = _loggerFactory.Create())
            {
                logger.Log(string.Format("LED {0} is off", Id));
                _led.SetRGB(0, 0, 0);
            }
        }

        public void On()
        {
            using (var logger = _loggerFactory.Create())
            {
                logger.Log(string.Format("LED {0} is on", Id));
                _led.SetRGB(255, 255, 255);
            }
        }
    }

    public class Button
    {
        ISwitch _switch;

        public Button(ISwitch s)
        {
            _switch = s;
        }

        public void Click()
        {
            if (!_switch.IsOn)
            {
                _switch.On();
            }
            else
            {
                _switch.Off();
            }

            _switch.IsOn = !_switch.IsOn;
        }
    }

    class Program
    {       
        static void Main(string[] args)
        {
            var container = new Container();
            ContainerBootstrap.RegisterTypes(container);

            //Button btn = new Button(new LedSwitch(new LedLight()));
            Button btn = container.GetInstance<Button>();

            btn.Click();
            btn.Click();
            btn.Click();
            btn.Click();
            btn.Click();
        }
    }

    internal class ContainerBootstrap
    {
        internal static void RegisterTypes(Container container)
        {
            container.Register<ILoggerFactory, DbLoggerFactory>();
            container.Register<ISwitch>(() => new LedSwitch(container.GetInstance<LedLight>(), "00-01", container.GetInstance<ILoggerFactory>()));            
        }
    }
}
