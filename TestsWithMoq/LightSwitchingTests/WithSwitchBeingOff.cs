﻿using LightSwitching;
using Moq;
using NUnit.Framework;

namespace LightSwitchingTests
{
    [TestFixture]
    public class WithSwitchBeingOff
    {
        Mock<ISwitch> mq_switch;
        Button btn;

        [SetUp]
        public void SetUp()
        {
            // Arrange
            mq_switch = new Mock<ISwitch>();
            btn = new Button(mq_switch.Object);
        }

        [Test]
        public void WhenButtonIsClickedLighSwitchtIsOn()
        {                   
            // Act
            btn.Click();

            // Assert
            mq_switch.Verify(m => m.On());
        }

        [Test]
        public void WhenButtonIsClickedStateIsSetToOn()
        {
            // Act
            btn.Click();

            // Assert
            mq_switch.VerifySet(m => m.IsOn = true);
        }  
    }
}
