﻿using LightSwitching;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LightSwitchingTests
{
    [TestFixture]
    public class WhenSwitchBeingOn
    {
        private Mock<ISwitch> mq_switch;
        private Button btn;

        [SetUp]
        public void Setup()
        {
            mq_switch = new Mock<ISwitch>();
            mq_switch.Setup(m => m.IsOn).Returns(true);
            btn = new Button(mq_switch.Object);
        }

        [Test]
        public void WhenButtonIsClickedLightSwitchIsOff()
        {
            btn.Click();

            mq_switch.Verify(m => m.Off());
        }

        [Test]
        public void WhenButtonIsClickedStateIsSetToOff()
        {
            btn.Click();

            mq_switch.VerifySet(m => m.IsOn = false);
        }
    }
}
