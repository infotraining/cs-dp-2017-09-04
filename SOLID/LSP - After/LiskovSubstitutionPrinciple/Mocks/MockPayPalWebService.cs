﻿namespace LiskovSubstitutionPrinciple.Mocks
{
    public class MockPayPalWebService
    {
        public string ObtainToken(string AccountName, string Password)
        {
            return "";
        }

        public string MakeRefund(decimal amount, string transactionId, string token)
        {
            return "Auth:0999";
        }
    }
}