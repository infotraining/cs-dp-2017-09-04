﻿using System;

namespace LiskovSubstitutionPrinciple
{
    public class PaymentServiceFactory
    {
        public static PaymentServiceBase GetPaymentServiceFrom(PaymentType paymentType)
        {
            switch (paymentType)
            {
                case PaymentType.PayPal:
                    return new PayPalPayment("Scott123-PP", "Scott123-PP");
                case PaymentType.WorldPay:
                    return new WorldPayPayment("Scott123-WP", "ASDASDP", "1");
                default:
                    throw new ApplicationException("No Payment Service available for " + paymentType.ToString());
            }
        }
    }
}