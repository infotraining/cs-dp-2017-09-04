﻿using System;

namespace DependencyInjection.Dependencies
{
    public interface ILoggingDataSink
    {
        void Write(string message);
    }
    public class LoggingDataSink : ILoggingDataSink
    {
        public void Write(string message)
        {
            Console.WriteLine("Logging in console: {0}", message);
        }
    }
}
