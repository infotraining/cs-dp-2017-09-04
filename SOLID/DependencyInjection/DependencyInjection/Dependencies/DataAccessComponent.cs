﻿using System;

namespace DependencyInjection.Dependencies
{
    public interface IDataAccessComponent
    {
        string GetSymbol(int id);
    }

    public class DataAccessComponent : IDataAccessComponent
    {
        private string _sqlConnStr;

        public DataAccessComponent(string sqlConnStr)
        {
            this._sqlConnStr = sqlConnStr;
        }

        public string GetSymbol(int id)
        {
            Console.WriteLine("Connecting to DB: {0}", _sqlConnStr);

            return "IBM";
        }
    }
}
