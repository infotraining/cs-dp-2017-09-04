﻿using System;
using System.Configuration;
using DependencyInjection.Dependencies;

namespace DependencyInjection.BeforeDI
{
    public class BusinessService
    {
        private readonly string _databaseConnectionString =
            ConfigurationManager.ConnectionStrings["MyConnectionString"].ConnectionString;
        private readonly string _webServiceAddress =
            ConfigurationManager.AppSettings["MyWebServiceAddress"];
        private readonly ILoggingDataSink _loggingDataSink;

        private DataAccessComponent _dataAccessComponent;
        private WebServiceProxy _webServiceProxy;
        private LoggingComponent _loggingComponent;

        public BusinessService()
        {
            _loggingDataSink = new LoggingDataSink();
            _loggingComponent = new LoggingComponent(_loggingDataSink, "AppLog");
            _webServiceProxy = new WebServiceProxy(_webServiceAddress);
            _dataAccessComponent = new DataAccessComponent(_databaseConnectionString);
        }

        public decimal GetPriceById(int id)
        {
            _loggingComponent.LogMessage(string.Format("GetPriceById({0}) : {1}", id, DateTime.Now));
            string symbol = _dataAccessComponent.GetSymbol(id);
            decimal price = _webServiceProxy.GetPrice(symbol);
            return price;
        }
    }
}
