﻿using System;
using DependencyInjection.SimpleInjectorBootstraper;
using DependencyInjection.UnityBootstrapper;
using Microsoft.Practices.Unity;
using SimpleInjector;
using BusinessService = DependencyInjection.AfterDI.BusinessService;

namespace DependencyInjection
{
    class Program
    {
        static void Main(string[] args)
        {
            #region Without Dependency Injection
            {
                var service = new DependencyInjection.BeforeDI.BusinessService();

                decimal price = service.GetPriceById(10);

                Console.WriteLine("Price {0:c}", price);
            }
            #endregion

            Console.WriteLine("\n\n");

            #region Using Unity IoC        
            {
                UnityContainer ioc = new UnityContainer();
                ContainerBootstrapper.RegisterTypes(ioc);

                var service = ioc.Resolve<BusinessService>();
                // var service = ioc.Resolve<AfterDI.BusinessService>(new ParameterOverride("logName", "AlternativeAppData"));
                decimal price = service.GetPriceById(10);

                Console.WriteLine("Price {0:c}", price);
                
            }        
            #endregion
        
            Console.WriteLine("\n\n");

            #region Using SimpleInjector        
            {
                var container = new Container();

                ContainerBootstraper.RegisterTypes(container);

                var service = container.GetInstance<BusinessService>();
                var price = service.GetPriceById(10);

                Console.WriteLine("Price {0:c}", price);
            }
            #endregion
        }
    }
}
