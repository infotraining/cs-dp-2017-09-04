﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LiskovSubstitutionPrinciple.Mocks;

namespace LiskovSubstitutionPrinciple
{
    public class WorldPayPayment : PaymentServiceBase
    {
        public string AccountId { get; set; }
        public string AccountPassword { get; set; }
        public string ProductId { get; set; }

        public override string Refund(decimal amount, string transactionId)
        {
            MockWorldPayWebService worldPayWebService = new MockWorldPayWebService();

            string response = worldPayWebService.MakeRefund(amount, transactionId, AccountId, AccountPassword, ProductId);

            return response;
        }
    }
}
