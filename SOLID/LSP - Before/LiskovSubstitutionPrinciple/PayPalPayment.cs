﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LiskovSubstitutionPrinciple.Mocks;

namespace LiskovSubstitutionPrinciple
{
    public class PayPalPayment : PaymentServiceBase
    {
        public string AccountName { get; set; }
        public string Password { get; set; }

        public override string Refund(decimal amount, string transactionId)
        {
            MockPayPalWebService payPalWebService = new MockPayPalWebService();

            string token = payPalWebService.ObtainToken(AccountName, Password);

            string response = payPalWebService.MakeRefund(amount, transactionId, token);

            return response;
        }
    }
}
