﻿using System;

namespace LiskovSubstitutionPrinciple
{
    public class PaymentServiceFactory
    {
        public static PaymentServiceBase GetPaymentServiceFrom(PaymentType paymentType)
        {
            switch (paymentType)
            {
                case PaymentType.PayPal:
                    return new PayPalPayment();
                case PaymentType.WorldPay:
                    return new WorldPayPayment();
                default:
                    throw new ApplicationException("No Payment Service available for " + paymentType.ToString());
            }
        }
    }
}