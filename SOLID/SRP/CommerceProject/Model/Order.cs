﻿using System;
using System.ComponentModel;

namespace CommerceProject.Model
{
    public abstract class Order
    {
        protected Cart _cart;

        public Order(Cart cart)
        {
            _cart = cart;
        }

        public abstract void Checkout();
    }

    public class OnlineOrder : Order
    {
        private PaymentDetails _paymentDetails;
        private IPaymentProcessor _paymentProcessor;
        private IReservationService _reservationService;
        private INotificationService _notificationService;

        public OnlineOrder(Cart cart, PaymentDetails paymentDetails, IPaymentProcessor paymentProcessor, IReservationService reservationService, INotificationService notificationService) : base(cart)
        {
            _paymentDetails = paymentDetails;
            _paymentProcessor = paymentProcessor;
            _reservationService = reservationService;
            _notificationService = notificationService;
        }

        public override void Checkout()
        {
            _paymentProcessor.ProcessCreditCard(_paymentDetails, _cart.TotalAmount);

            _reservationService.ReserveInventory(_cart.Items);

            _notificationService.NotifyCustomerOrderCreated(_cart);
        }
    }

    public class PoSCreditOrder : Order
    {
        private PaymentDetails _paymentDetails;
        private IPaymentProcessor _paymentProcessor;

        public PoSCreditOrder(Cart cart, PaymentDetails paymentDetails, IPaymentProcessor paymentProcessor) : base(cart)
        {
            _paymentProcessor = paymentProcessor;
            _paymentDetails = paymentDetails;
        }

        public override void Checkout()
        {
            _paymentProcessor.ProcessCreditCard(_paymentDetails, _cart.TotalAmount);
        }
    }

    public class PoSCashOrder : Order
    {
        public PoSCashOrder(Cart cart) : base(cart)
        {
        }

        public override void Checkout()
        {
        }
    }

    public class OrderException : Exception
    {
        public OrderException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}