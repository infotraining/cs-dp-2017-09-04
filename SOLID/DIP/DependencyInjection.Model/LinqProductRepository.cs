﻿using System.Collections.Generic;

namespace DependencyInversionPrinciple.Model
{
    public class LinqProductRepository : IProductRepository
    {
        public IEnumerable<Product> FindAll()
        {            
            return new List<Product>();
        }     
    }
}
