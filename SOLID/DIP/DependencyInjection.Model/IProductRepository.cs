﻿using System.Collections.Generic;

namespace DependencyInversionPrinciple.Model
{
    public interface IProductRepository
    {
        IEnumerable<Product> FindAll();
    }
}
