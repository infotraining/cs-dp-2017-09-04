﻿namespace DependencyInversionPrinciple.Model
{
    public interface IProductDiscountStrategy
    {
        decimal Discount { get; }
    }
}
