﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NUnitDemo
{
    [TestFixture]
    public class ListTests
    {
        [Test]
        public void AddingAnItemToListChangesSize()
        {
            // Arrange
            List<int> lst = new List<int>();

            // Act
            lst.Add(1);

            // Assert
            Assert.AreEqual(1, lst.Count);
        }
    }
}


