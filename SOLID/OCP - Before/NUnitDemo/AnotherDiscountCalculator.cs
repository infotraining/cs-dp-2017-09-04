﻿using System;

namespace NUnitDemo
{
    public class AnotherDiscountCalculator
    {
        public decimal CalculateDiscount(Customer c, decimal amount)
        {
            if (c.IsVeteran)
                return amount * 0.9M;

            return amount;
        }
    }
}