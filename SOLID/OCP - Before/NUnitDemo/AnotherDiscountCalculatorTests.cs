﻿using System;
using NUnit.Framework;

namespace NUnitDemo
{
    [TestFixture]
    public class AnotherDiscountCalculatorTests
    {
        protected decimal amount;
        AnotherDiscountCalculator discountCalculator;

        [SetUp]
        public void Setup()
        {
            amount = 100.0M;
            discountCalculator = new AnotherDiscountCalculator();
        }

        [Test]
        public void ForPlainCusomerNoDiscount()
        {
            // Arrange
            Customer c = new Customer();
            
            // Act
            var after_discount = discountCalculator.CalculateDiscount(c, amount);

            // Assert
            Assert.AreEqual(amount, after_discount);
        }        

        [Test]
        public void ForVeteran10PercentDiscount()
        {
            // Arrange
            Customer c = new Customer() { IsVeteran = true };            

            // Act
            var after_discount = discountCalculator.CalculateDiscount(c, amount);

            // Assert
            Assert.AreEqual(after_discount, 0.9M * amount);
        }
    }
}


